import React, { useRef, useEffect, useState } from "react";
import "./App.css";
import FirstPanel from "./component/firstPanel";
import SecondPanel from "./component/secondPanel";
import ThirdPanel from "./component/thirdPanel";
import WelcomeDiv from "./component/welcomeDiv";
import { TimelineLite } from "gsap";

let tl = new TimelineLite();

const VISIBLE_DIV = [
  <WelcomeDiv timeline={tl} />,
  <FirstPanel timeline={tl} />,
  <SecondPanel />,
  <ThirdPanel />,
];

const Main = () => {
  const [currDiv, setCurrDiv] = useState(0);
  const scrollRef = useRef();
  // let newRef = useRef();

  // let tl = new TimelineLite();
  console.log("event", currDiv);

  const scrollEvent = (e) => {
    setCurrDiv((cd) => {
      if (e.deltaY > 0 && cd < VISIBLE_DIV.length - 1) return cd + 1;
      else if (e.deltaY < 0 && cd > 0) return cd - 1;
      return cd;
    });
  };

  useEffect(() => {
    if (scrollRef) window.addEventListener("wheel", scrollEvent);
    return () => {
      window.removeEventListener("wheel", scrollEvent);
    };
  }, []);

  return (
    <div ref={scrollRef} className="container">
      {/* className="App" */}
      {/* {VISIBLE_DIV[currDiv]} */}
      {currDiv === 0 && <WelcomeDiv timeline={tl} visible={currDiv === 0} />}
      <FirstPanel timeline={tl} visible={currDiv === 1} />
      <SecondPanel visible />
      <ThirdPanel visible />
    </div>
  );
};

export default Main;
