import React, { useEffect } from "react";
// import { TimelineLite } from "gsap";

const WelcomeDiv = ({ timeline: tl, visible }) => {
  // let tl = new TimelineLite();

  useEffect(() => {
    console.log("welcome div");
    // if (visible) {
    //.from("section", 0.7, { x: window.innerWidth * 1 })
    tl.from("body", 0.5, { backgroundColor: "#282c34" })
      .from("section", { backgroundColor: "#282c34" })
      .from("section h1", 0.5, {
        y: window.innerHeight * 1,
      })
      .from("section p", 0.5, { y: window.innerHeight * -1 });
    // setTimeout(() => {
    //   tl.to("section", 1, { x: window.innerWidth * 1 });
    // }, -1000);
    return () => {
      // setTimeout(() => {
      tl.to("body", 0.1, { backgroundColor: "#282c34" }).to("body", 0.5, {
        backgroundColor: "yellow",
      });
      // }, -1000);
      // // tl.to("section", 1, { x: window.innerWidth * 1 });
      // tl.to("section", 1, { x: window.innerWidth * 1 });
    };
    // }
  }, [tl]);

  return (
    visible && (
      <section>
        <h1>Welcome To Blogger</h1>
        <p>Scroll To Begin</p>
      </section>
    )
  );
};

export default WelcomeDiv;
