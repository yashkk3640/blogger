import React, { useEffect } from "react";
import { Bounce } from "gsap";

// scrollTrigger: {
//   trigger: ".box",
//   start: "center center",
//   end: "bottom top",
//   marker: true,
//   scrub: true,
// },

const FirstPanel = ({ timeline: tl, visible }) => {
  // let tl = new TimelineLite();

  // to("section", 1, { x: window.innerWidth * 1 });
  useEffect(() => {
    console.log("first panel", visible);
    visible &&
      tl
        .from(".box", 1, { y: window.innerHeight * 1 })
        .from(".box h2", 1, {
          x: window.innerWidth * 1.5,
          ease: Bounce.easeInOut,
        })
        .from(".box img", 1, {
          x: window.innerWidth * -1,
          scale: 0,
          rotate: 360,
        });
    // return () => {
    //   tl.from(".box", 5, { opacity: 0 });
    // };
  }, [tl, visible]);

  return (
    visible && (
      <div className="box">
        <h2>Yash Khatri</h2>
        <h2>Computer Engineer</h2>
        <h2>Welcome to gsap with react !!</h2>
        <h2>How it is ?</h2>
        <img className="logo" src="favicon.jpg" alt="y@$h" />
      </div>
    )
  );
};

export default FirstPanel;
